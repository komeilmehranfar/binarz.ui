import React from 'react';
import star from '../../assets/images/star.svg'

function Ticker(props){
    const { data } = props; 
    return (
        <>
            { data == 'usd' &&
                <table className="table table-striped">
                    <thead>
                        <tr>
                            <th scope="col">Coin</th>
                            <th scope="col">Last Price</th>
                            <th scope="col">Change</th>
                        </tr>
                    </thead>
                    <tbody className="crypt-table-hover">
                        <tr>
                            <td className="align-middle"><img className="crypt-star pr-1" alt="star" src={star} width="15" /> BTC/USDT</td>
                            <td className="crypt-down align-middle"><span className="pr-2" data-toggle="tooltip" data-placement="right" title="$ 0.05">0.00004356</span></td>
                            <td> <span className="d-block">5.3424984</span> <b className="crypt-down">-5.4%</b> </td>
                        </tr>
                        <tr>
                            <td className="align-middle"><img className="crypt-star pr-1" alt="star" src={star} width="15" /> LTC/USDT</td>
                            <td className="crypt-down align-middle"><span className="pr-2" data-toggle="tooltip" data-placement="right" title="$ 0.05">0.00005640</span></td>
                            <td> <span className="d-block">6.6768876</span> <span>6.7%</span> </td>
                        </tr>
                        <tr>
                            <td className="align-middle"><img className="crypt-star pr-1" alt="star" src={star} width="15" /> ETH/USDT</td>
                            <td className="crypt-down align-middle"><span className="pr-2" data-toggle="tooltip" data-placement="right" title="$ 0.05">0.00002340</span></td>
                            <td> <span className="d-block">6.6768876</span> <b className="crypt-down">-7.7%</b> </td>
                        </tr>
                        <tr>
                            <td className="align-middle"><img className="crypt-star pr-1" alt="star" src={star} width="15" /> DOGE/USDT</td>
                            <td className="crypt-up align-middle"><span className="pr-2" data-toggle="tooltip" data-placement="right" title="$ 0.05">0.00003644</span></td>
                            <td> <span className="d-block">6.6768876</span> <b className="crypt-up">+3.7%</b> </td>
                        </tr>
                        <tr>
                            <td className="align-middle"><img className="crypt-star pr-1" alt="star" src={star} width="15" /> XMR/USDT</td>
                            <td className="align-middle"><span className="pr-2" data-toggle="tooltip" data-placement="right" title="$ 0.05">0.00063440</span></td>
                            <td> <span className="d-block">6.6768876</span> <span>3.7%</span> </td>
                        </tr>
                        <tr>
                            <td className="align-middle"><img className="crypt-star pr-1" alt="star" src={star} width="15" /> ERC20/USDT</td>
                            <td className="align-middle"><span className="pr-2" data-toggle="tooltip" data-placement="right" title="$ 0.05">0.000344</span></td>
                            <td> <span className="d-block">6.6768876</span> <span className="crypt-up"><b>+3.7%</b></span> </td>
                        </tr>
                        <tr>
                            <td className="align-middle"><img className="crypt-star pr-1" alt="star" src={star} width="15" /> CFT/USDT</td>
                            <td className="crypt-up align-middle"><span className="pr-2" data-toggle="tooltip" data-placement="right" title="$ 0.05">0.00003644</span></td>
                            <td> <span className="d-block">6.6768876</span> <b className="crypt-up">+3.7%</b> </td>
                        </tr>
                        <tr>
                            <td className="align-middle"><img className="crypt-star pr-1" alt="star" src={star} width="15" /> RIF/USDT</td>
                            <td className="align-middle"><span className="pr-2" data-toggle="tooltip" data-placement="right" title="$ 0.05">0.000344</span></td>
                            <td> <span className="d-block">6.6768876</span> <span className="crypt-up"><b>+3.7%</b></span> </td>
                        </tr>
                        <tr>
                            <td className="align-middle"><img className="crypt-star pr-1" alt="star" src={star} width="15" /> NEO/USDT</td>
                            <td className="align-middle"><span className="pr-2" data-toggle="tooltip" data-placement="right" title="$ 0.05">0.56723</span></td>
                            <td> <span className="d-block">9.34546</span> <span>6.7%</span> </td>
                        </tr>
                        <tr>
                            <td className="align-middle"><img className="crypt-star pr-1" alt="star" src={star} width="15" /> MXM/USDT</td>
                            <td className="crypt-up align-middle"><span className="pr-2" data-toggle="tooltip" data-placement="right" title="$ 0.05">0.00003644</span></td>
                            <td> <span className="d-block">6.6768876</span> <b className="crypt-up">+3.7%</b> </td>
                        </tr>
                        <tr>
                            <td className="align-middle"><img className="crypt-star pr-1" alt="star" src={star} width="15" /> LSK/USDT</td>
                            <td className="align-middle"><span className="pr-2" data-toggle="tooltip" data-placement="right" title="$ 0.05">0.56723</span></td>
                            <td> <span className="d-block">9.34546</span> <span>6.7%</span> </td>
                        </tr>
                        <tr>
                            <td className="align-middle"><img className="crypt-star pr-1" alt="star" src={star} width="15" /> XRP/USDT</td>
                            <td className="align-middle"><span className="pr-2" data-toggle="tooltip" data-placement="right" title="$ 0.05">0.000344</span></td>
                            <td> <span className="d-block">6.6768876</span> <span className="crypt-up"><b>+3.7%</b></span> </td>
                        </tr>
                        <tr>
                            <td className="align-middle"><img className="crypt-star pr-1" alt="star" src={star} width="15" /> CXC/USDT</td>
                            <td className="align-middle"><span className="pr-2" data-toggle="tooltip" data-placement="right" title="$ 0.05">0.000344</span></td>
                            <td> <span className="d-block">6.6768876</span> <span className="crypt-up"><b>+3.7%</b></span> </td>
                        </tr>
                        <tr>
                            <td className="align-middle"><img className="crypt-star pr-1" alt="star" src={star} width="15" /> HUP/USDT</td>
                            <td className="align-middle"><span className="pr-2" data-toggle="tooltip" data-placement="right" title="$ 0.05">0.56723</span></td>
                            <td> <span className="d-block">9.34546</span> <span>6.7%</span> </td>
                        </tr>
                        <tr>
                            <td className="align-middle"><img className="crypt-star pr-1" alt="star" src={star} width="15" /> TRX/USDT</td>
                            <td className="crypt-up align-middle"><span className="pr-2" data-toggle="tooltip" data-placement="right" title="$ 0.05">0.00003644</span></td>
                            <td> <span className="d-block">6.6768876</span> <b className="crypt-up">+3.7%</b> </td>
                        </tr>
                        <tr>
                            <td className="align-middle"><img className="crypt-star pr-1" alt="star" src={star} width="15" /> ODC/USDT</td>
                            <td className="align-middle"><span className="pr-2" data-toggle="tooltip" data-placement="right" title="$ 0.05">0.56723</span></td>
                            <td> <span className="d-block">9.34546</span> <span>6.7%</span> </td>
                        </tr>
                        <tr>
                            <td className="align-middle"><img className="crypt-star pr-1" alt="star" src={star} width="15" /> AIPE/USDT</td>
                            <td className="align-middle"><span className="pr-2" data-toggle="tooltip" data-placement="right" title="$ 0.05">0.000344</span></td>
                            <td> <span className="d-block">6.6768876</span> <span className="crypt-up"><b>+3.7%</b></span> </td>
                        </tr>
                        <tr>
                            <td className="align-middle"><img className="crypt-star pr-1" alt="star" src={star} width="15" /> B91/USDT</td>
                            <td className="align-middle"><span className="pr-2" data-toggle="tooltip" data-placement="right" title="$ 0.05">0.000344</span></td>
                            <td> <span className="d-block">6.6768876</span> <span className="crypt-up"><b>+3.7%</b></span> </td>
                        </tr>
                        <tr>
                            <td className="align-middle"><img className="crypt-star pr-1" alt="star" src={star} width="15" /> BGC/USDT</td>
                            <td className="align-middle"><span className="pr-2" data-toggle="tooltip" data-placement="right" title="$ 0.05">0.56723</span></td>
                            <td> <span className="d-block">9.34546</span> <span>6.7%</span> </td>
                        </tr>
                    <tr>
                            <td className="align-middle"><img className="crypt-star pr-1" alt="star" src={star} width="15" /> GOM/USDT</td>
                            <td className="crypt-down align-middle"><span className="pr-2" data-toggle="tooltip" data-placement="right" title="$ 0.05">0.00004356</span></td>
                            <td> <span className="d-block">5.3424984</span> <b className="crypt-down">-5.4%</b> </td>
                        </tr>
                        <tr>
                            <td className="align-middle"><img className="crypt-star pr-1" alt="star" src={star} width="15" /> RBZ/USDT</td>
                            <td className="crypt-down align-middle"><span className="pr-2" data-toggle="tooltip" data-placement="right" title="$ 0.05">0.00005640</span></td>
                            <td> <span className="d-block">6.6768876</span> <span>6.7%</span> </td>
                        </tr>
                        <tr>
                            <td className="align-middle"><img className="crypt-star pr-1" alt="star" src={star} width="15" /> CUST/USDT</td>
                            <td className="crypt-down align-middle"><span className="pr-2" data-toggle="tooltip" data-placement="right" title="$ 0.05">0.00002340</span></td>
                            <td> <span className="d-block">6.6768876</span> <b className="crypt-down">-7.7%</b> </td>
                        </tr>
                        <tr>
                            <td className="align-middle"><img className="crypt-star pr-1" alt="star" src={star} width="15" /> GRAM/USDT</td>
                            <td className="align-middle"><span className="pr-2" data-toggle="tooltip" data-placement="right" title="$ 0.05">0.000344</span></td>
                            <td> <span className="d-block">6.6768876</span> <span className="crypt-up"><b>+3.7%</b></span> </td>
                        </tr>
                        <tr>
                            <td className="align-middle"><img className="crypt-star pr-1" alt="star" src={star} width="15" /> TVB/USDT</td>
                            <td className="crypt-up align-middle"><span className="pr-2" data-toggle="tooltip" data-placement="right" title="$ 0.05">0.00003644</span></td>
                            <td> <span className="d-block">6.6768876</span> <b className="crypt-up">+3.7%</b> </td>
                        </tr>
                        <tr>
                            <td className="align-middle"><img className="crypt-star pr-1" alt="star" src={star} width="15" /> TIMO/USDT</td>
                            <td className="align-middle"><span className="pr-2" data-toggle="tooltip" data-placement="right" title="$ 0.05">0.56723</span></td>
                            <td> <span className="d-block">9.34546</span> <span>6.7%</span> </td>
                        </tr>
                        <tr>
                            <td className="align-middle"><img className="crypt-star pr-1" alt="star" src={star} width="15" /> CCE/USDT</td>
                            <td className="align-middle"><span className="pr-2" data-toggle="tooltip" data-placement="right" title="$ 0.05">0.56723</span></td>
                            <td> <span className="d-block">9.34546</span> <span>6.7%</span> </td>
                        </tr>
                    <tr>
                            <td className="align-middle"><img className="crypt-star pr-1" alt="star" src={star} width="15" /> QTUM/USDT</td>
                            <td className="crypt-down align-middle"><span className="pr-2" data-toggle="tooltip" data-placement="right" title="$ 0.05">0.00004356</span></td>
                            <td> <span className="d-block">5.3424984</span> <b className="crypt-down">-5.4%</b> </td>
                        </tr>
                        <tr>
                            <td className="align-middle"><img className="crypt-star pr-1" alt="star" src={star} width="15" /> PAX/USDT</td>
                            <td className="crypt-down align-middle"><span className="pr-2" data-toggle="tooltip" data-placement="right" title="$ 0.05">0.00005640</span></td>
                            <td> <span className="d-block">6.6768876</span> <span>6.7%</span> </td>
                        </tr>
                        <tr>
                            <td className="align-middle"><img className="crypt-star pr-1" alt="star" src={star} width="15" /> CS/USDT</td>
                            <td className="crypt-down align-middle"><span className="pr-2" data-toggle="tooltip" data-placement="right" title="$ 0.05">0.00002340</span></td>
                            <td> <span className="d-block">6.6768876</span> <b className="crypt-down">-7.7%</b> </td>
                        </tr>
                        <tr>
                            <td className="align-middle"><img className="crypt-star pr-1" alt="star" src={star} width="15" /> HNB/USDT</td>
                            <td className="align-middle"><span className="pr-2" data-toggle="tooltip" data-placement="right" title="$ 0.05">0.56723</span></td>
                            <td> <span className="d-block">9.34546</span> <span>6.7%</span> </td>
                        </tr>
                        <tr>
                            <td className="align-middle"><img className="crypt-star pr-1" alt="star" src={star} width="15" /> FTN/USDT</td>
                            <td className="crypt-up align-middle"><span className="pr-2" data-toggle="tooltip" data-placement="right" title="$ 0.05">0.00003644</span></td>
                            <td> <span className="d-block">6.6768876</span> <b className="crypt-up">+3.7%</b> </td>
                        </tr>
                        <tr>
                            <td className="align-middle"><img className="crypt-star pr-1" alt="star" src={star} width="15" /> MZG/USDT</td>
                            <td className="align-middle"><span className="pr-2" data-toggle="tooltip" data-placement="right" title="$ 0.05">0.000344</span></td>
                            <td> <span className="d-block">6.6768876</span> <span className="crypt-up"><b>+3.7%</b></span> </td>
                        </tr>
            
                    </tbody>
                </table>
            }
            { data == 'btc' &&
            <table className="table table-striped">
                    <thead>
                        <tr>
                            <th scope="col">Coin</th>
                            <th scope="col">Last Price</th>
                            <th scope="col">Change</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td className="align-middle"><img className="crypt-star pr-1" alt="star" src={star} width="15" /> ETH/BTC</td>
                            <td className="align-middle"><span className="pr-2" data-toggle="tooltip" data-placement="right" title="$ 0.05">0.0000234</span></td>
                            <td> <span className="d-block">6.6768876</span> <b className="crypt-down">-7.7%</b> </td>
                        </tr>
                        <tr>
                            <td className="align-middle"><img className="crypt-star pr-1" alt="star" src={star} width="15" /> EOS/BTC</td>
                            <td className="crypt-down align-middle"><span className="pr-2" data-toggle="tooltip" data-placement="right" title="$ 0.05">0.000056</span></td>
                            <td> <span className="d-block">5.3424984</span> <b className="crypt-down">-5.4%</b> </td>
                        </tr>
                        <tr>
                            <td className="align-middle"><img className="crypt-star pr-1" alt="star" src={star} width="15" /> LTC/BTC</td>
                            <td className="align-middle"><span className="pr-2" data-toggle="tooltip" data-placement="right" title="$ 0.05">0.0000564</span></td>
                            <td> <span className="d-block">6.6768876</span> <span>6.7%</span> </td>
                        </tr>
                        <tr>
                            <td className="align-middle"><img className="crypt-star pr-1" alt="star" src={star} width="15" /> DOGE/BTC</td>
                            <td className="align-middle"><span className="pr-2" data-toggle="tooltip" data-placement="right" title="$ 0.05">0.000344</span></td>
                            <td> <span className="d-block">6.6768876</span> <b className="crypt-up">+3.7%</b> </td>
                        </tr>
                        <tr>
                            <td className="align-middle"><img className="crypt-star pr-1" alt="star" src={star} width="15" /> XMR/BTC</td>
                            <td className="align-middle"><span className="pr-2" data-toggle="tooltip" data-placement="right" title="$ 0.05">0.000344</span></td>
                            <td> <span className="d-block">6.6768876</span> <span>3.7%</span> </td>
                        </tr>
                        <tr>
                            <td className="align-middle"><img className="crypt-star pr-1" alt="star" src={star} width="15" /> LINK/BTC</td>
                            <td className="align-middle"><span className="pr-2" data-toggle="tooltip" data-placement="right" title="$ 0.05">0.000344</span></td>
                            <td> <span className="d-block">6.6768876</span> <span className="crypt-up"><b>+3.7%</b></span> </td>
                        </tr>
                        <tr>
                            <td className="align-middle"><img className="crypt-star pr-1" alt="star" src={star} width="15" /> FTN/BTC</td>
                            <td className="align-middle"><span className="pr-2" data-toggle="tooltip" data-placement="right" title="$ 0.05">0.000344</span></td>
                            <td> <span className="d-block">6.6768876</span> <span className="crypt-up"><b>+3.7%</b></span> </td>
                        </tr>
                        <tr>
                            <td className="align-middle"><img className="crypt-star pr-1" alt="star" src={star} width="15" /> RIF/BTC</td>
                            <td className="align-middle"><span className="pr-2" data-toggle="tooltip" data-placement="right" title="$ 0.05">0.000344</span></td>
                            <td> <span className="d-block">6.6768876</span> <span className="crypt-up"><b>+3.7%</b></span> </td>
                        </tr>
                        <tr>
                            <td className="align-middle"><img className="crypt-star pr-1" alt="star" src={star} width="15" /> NEO/BTC</td>
                            <td className="align-middle"><span className="pr-2" data-toggle="tooltip" data-placement="right" title="$ 0.05">0.56723</span></td>
                            <td> <span className="d-block">9.34546</span> <span>6.7%</span> </td>
                        </tr>
                        <tr>
                            <td className="align-middle"><img className="crypt-star pr-1" alt="star" src={star} width="15" /> TRX/BTC</td>
                            <td className="align-middle"><span className="pr-2" data-toggle="tooltip" data-placement="right" title="$ 0.05">0.56723</span></td>
                            <td> <span className="d-block">9.34546</span> <span>6.7%</span> </td>
                        </tr>
                        <tr>
                            <td className="align-middle"><img className="crypt-star pr-1" alt="star" src={star} width="15" /> LSK/BTC</td>
                            <td className="align-middle"><span className="pr-2" data-toggle="tooltip" data-placement="right" title="$ 0.05">0.56723</span></td>
                            <td> <span className="d-block">9.34546</span> <span>6.7%</span> </td>
                        </tr>
                        <tr>
                            <td className="align-middle"><img className="crypt-star pr-1" alt="star" src={star} width="15" /> XRP/BTC</td>
                            <td className="align-middle"><span className="pr-2" data-toggle="tooltip" data-placement="right" title="$ 0.05">0.000344</span></td>
                            <td> <span className="d-block">6.6768876</span> <span className="crypt-up"><b>+3.7%</b></span> </td>
                        </tr>
                        <tr>
                            <td className="align-middle"><img className="crypt-star pr-1" alt="star" src={star} width="15" /> CNB/BTC</td>
                            <td className="align-middle"><span className="pr-2" data-toggle="tooltip" data-placement="right" title="$ 0.05">0.56723</span></td>
                            <td> <span className="d-block">9.34546</span> <span>6.7%</span> </td>
                        </tr>
                        <tr>
                            <td className="align-middle"><img className="crypt-star pr-1" alt="star" src={star} width="15" /> VEN/BTC</td>
                            <td className="align-middle"><span className="pr-2" data-toggle="tooltip" data-placement="right" title="$ 0.05">0.56723</span></td>
                            <td> <span className="d-block">9.34546</span> <span>6.7%</span> </td>
                        </tr>
                        <tr>
                            <td className="align-middle"><img className="crypt-star pr-1" alt="star" src={star} width="15" /> DASH/BTC</td>
                            <td className="align-middle"><span className="pr-2" data-toggle="tooltip" data-placement="right" title="$ 0.05">0.56723</span></td>
                            <td> <span className="d-block">9.34546</span> <span>6.7%</span> </td>
                        </tr>
                    </tbody>
                </table>
            }
            { data == 'eth' &&
            <table className="table table-striped">
                    <thead>
                        <tr>
                            <th scope="col">Coin</th>
                            <th scope="col">Last Price</th>
                            <th scope="col">Change</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td className="align-middle"><img className="crypt-star pr-1" alt="star" src={star} width="15" /> BTC/ETH</td>
                            <td className="crypt-down align-middle"><span className="pr-2" data-toggle="tooltip" data-placement="right" title="$ 0.05">0.000056</span></td>
                            <td> <span className="d-block">5.3424984</span> <b className="crypt-down">-5.4%</b> </td>
                        </tr>
                        <tr>
                            <td className="align-middle"><img className="crypt-star pr-1" alt="star" src={star} width="15" /> LTC/ETH</td>
                            <td className="align-middle"><span className="pr-2" data-toggle="tooltip" data-placement="right" title="$ 0.05">0.0000564</span></td>
                            <td> <span className="d-block">6.6768876</span> <span>6.7%</span> </td>
                        </tr>
                        <tr>
                            <td className="align-middle"><img className="crypt-star pr-1" alt="star" src={star} width="15" /> ERC20/ETH</td>
                            <td className="align-middle"><span className="pr-2" data-toggle="tooltip" data-placement="right" title="$ 0.05">0.0000234</span></td>
                            <td> <span className="d-block">6.6768876</span> <b className="crypt-down">-7.7%</b> </td>
                        </tr>
                        <tr>
                            <td className="align-middle"><img className="crypt-star pr-1" alt="star" src={star} width="15" /> DOGE/ETH</td>
                            <td className="align-middle"><span className="pr-2" data-toggle="tooltip" data-placement="right" title="$ 0.05">0.000344</span></td>
                            <td> <span className="d-block">6.6768876</span> <b className="crypt-up">+3.7%</b> </td>
                        </tr>
                        <tr>
                            <td className="align-middle"><img className="crypt-star pr-1" alt="star" src={star} width="15" /> XMR/ETH</td>
                            <td className="align-middle"><span className="pr-2" data-toggle="tooltip" data-placement="right" title="$ 0.05">0.000344</span></td>
                            <td> <span className="d-block">6.6768876</span> <span>3.7%</span> </td>
                        </tr>
                        <tr>
                            <td className="align-middle"><img className="crypt-star pr-1" alt="star" src={star} width="15" /> HMB/ETH</td>
                            <td className="align-middle"><span className="pr-2" data-toggle="tooltip" data-placement="right" title="$ 0.05">0.000344</span></td>
                            <td> <span className="d-block">6.6768876</span> <span className="crypt-up"><b>+3.7%</b></span> </td>
                        </tr>
                        <tr>
                            <td className="align-middle"><img className="crypt-star pr-1" alt="star" src={star} width="15" /> FTN/ETH</td>
                            <td className="align-middle"><span className="pr-2" data-toggle="tooltip" data-placement="right" title="$ 0.05">0.000344</span></td>
                            <td> <span className="d-block">6.6768876</span> <span className="crypt-up"><b>+3.7%</b></span> </td>
                        </tr>
                        <tr>
                            <td className="align-middle"><img className="crypt-star pr-1" alt="star" src={star} width="15" /> MGC/ETH</td>
                            <td className="align-middle"><span className="pr-2" data-toggle="tooltip" data-placement="right" title="$ 0.05">0.000344</span></td>
                            <td> <span className="d-block">6.6768876</span> <span className="crypt-up"><b>+3.7%</b></span> </td>
                        </tr>
                        <tr>
                            <td className="align-middle"><img className="crypt-star pr-1" alt="star" src={star} width="15" /> IOTE/ETH</td>
                            <td className="align-middle"><span className="pr-2" data-toggle="tooltip" data-placement="right" title="$ 0.05">0.56723</span></td>
                            <td> <span className="d-block">9.34546</span> <span>6.7%</span> </td>
                        </tr>
                        <tr>
                            <td className="align-middle"><img className="crypt-star pr-1" alt="star" src={star} width="15" /> YTA/ETH</td>
                            <td className="align-middle"><span className="pr-2" data-toggle="tooltip" data-placement="right" title="$ 0.05">0.56723</span></td>
                            <td> <span className="d-block">9.34546</span> <span>6.7%</span> </td>
                        </tr>
                        <tr>
                            <td className="align-middle"><img className="crypt-star pr-1" alt="star" src={star} width="15" /> PQR/ETH</td>
                            <td className="align-middle"><span className="pr-2" data-toggle="tooltip" data-placement="right" title="$ 0.05">0.56723</span></td>
                            <td> <span className="d-block">9.34546</span> <span>6.7%</span> </td>
                        </tr>
                        <tr>
                            <td className="align-middle"><img className="crypt-star pr-1" alt="star" src={star} width="15" /> PAX/ETH</td>
                            <td className="align-middle"><span className="pr-2" data-toggle="tooltip" data-placement="right" title="$ 0.05">0.000344</span></td>
                            <td> <span className="d-block">6.6768876</span> <span className="crypt-up"><b>+3.7%</b></span> </td>
                        </tr>
                        <tr>
                            <td className="align-middle"><img className="crypt-star pr-1" alt="star" src={star} width="15" /> VBT/ETH</td>
                            <td className="align-middle"><span className="pr-2" data-toggle="tooltip" data-placement="right" title="$ 0.05">0.56723</span></td>
                            <td> <span className="d-block">9.34546</span> <span>6.7%</span> </td>
                        </tr>
                        <tr>
                            <td className="align-middle"><img className="crypt-star pr-1" alt="star" src={star} width="15" /> CCE/ETH</td>
                            <td className="align-middle"><span className="pr-2" data-toggle="tooltip" data-placement="right" title="$ 0.05">0.56723</span></td>
                            <td> <span className="d-block">9.34546</span> <span>6.7%</span> </td>
                        </tr>
                        <tr>
                            <td className="align-middle"><img className="crypt-star pr-1" alt="star" src={star} width="15" /> QTUM/ETH</td>
                            <td className="align-middle"><span className="pr-2" data-toggle="tooltip" data-placement="right" title="$ 0.05">0.56723</span></td>
                            <td> <span className="d-block">9.34546</span> <span>6.7%</span> </td>
                        </tr>
                        <tr>
                            <td className="align-middle"><img className="crypt-star pr-1" alt="star" src={star} width="15" /> BOA/ETH</td>
                            <td className="align-middle"><span className="pr-2" data-toggle="tooltip" data-placement="right" title="$ 0.05">0.56723</span></td>
                            <td> <span className="d-block">9.34546</span> <span>6.7%</span> </td>
                        </tr>
                    </tbody>
                </table>
            }
        </>
    )
}

export default Ticker;