import React from 'react';

export function TradingBox(){
    return (
        <div className="crypt-boxed-area">
            <h6 className="crypt-bg-head"><b className="crypt-up">BUY</b> / <b className="crypt-down">SELL</b></h6>
            <div className="row no-gutters">
                <div className="col-md-6">
                    <div className="crypt-buy-sell-form">
                        <p>Buy <span className="crypt-up">BTC</span> <span className="fright">Available: <b className="crypt-up">0.00 BTC</b></span></p>
                        <div className="crypt-buy">
                            <div className="input-group mb-3">
                                <div className="input-group-prepend"> <span className="input-group-text">Price</span> </div>
                                <input type="text" className="form-control" placeholder="0.02323476" readOnly />
                                <div className="input-group-append"> <span className="input-group-text">USDT</span> </div>
                            </div>
                            <div className="input-group mb-3">
                                <div className="input-group-prepend"> <span className="input-group-text">Amount</span> </div>
                                <input type="number" className="form-control"/>
                                <div className="input-group-append"> <span className="input-group-text">BTC</span> </div>
                            </div>
                            <div className="input-group mb-3">
                                <div className="input-group-prepend"> <span className="input-group-text">Total</span> </div>
                                <input type="text" className="form-control" readOnly/>
                                <div className="input-group-append"> <span className="input-group-text">BTC</span> </div>
                            </div>
                            <div>
                                <p>Fee: <span className="fright">100%x0.2=0.02</span></p>
                            </div>
                            <div className="text-center mt-3 mb-3 crypt-up">
                                <p>You will approximately pay</p>
                                <h4>0.09834 BTC</h4></div>
                            <div className="menu-green"><a href="" className="crypt-button-green-full">Buy</a></div>
                        </div>
                    </div>
                </div>
                <div className="col-md-6">
                    <div className="crypt-buy-sell-form">
                        <p>Sell <span className="crypt-down">BTC</span> <span className="fright">Available: <b className="crypt-down">0.00 BTC</b></span></p>
                        <div className="crypt-sell">
                            <div className="input-group mb-3">
                                <div className="input-group-prepend"> <span className="input-group-text">Price</span> </div>
                                <input type="text" className="form-control" placeholder="0.02323476" readOnly/>
                                <div className="input-group-append"> <span className="input-group-text">USDT</span> </div>
                            </div>
                            <div className="input-group mb-3">
                                <div className="input-group-prepend"> <span className="input-group-text">Amount</span> </div>
                                <input type="number" className="form-control"/>
                                <div className="input-group-append"> <span className="input-group-text">BTC</span> </div>
                            </div>
                            <div className="input-group mb-3">
                                <div className="input-group-prepend"> <span className="input-group-text">Total</span> </div>
                                <input type="text" className="form-control" readOnly/>
                                <div className="input-group-append"> <span className="input-group-text">BTC</span> </div>
                            </div>
                            <div>
                                <p>Fee: <span className="fright">100%x0.2=0.02</span></p>
                            </div>
                            <div className="text-center mt-3 mb-3 crypt-down">
                                <p>You will approximately pay</p>
                                <h4>0.09834 BTC</h4></div>
                            <div><a href="" className="crypt-button-red-full">Sell</a></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    
    )
}