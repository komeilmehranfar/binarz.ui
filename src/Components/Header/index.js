import React, {useState} from 'react';
import { Link } from 'react-router-dom';

function Header(){

    return <header>
        <div className="container-full-width">
            <div className="crypt-header">
                <div className="row">
                    <div className="col-xl-4 col-lg-4 col-md-4 col-sm-5">
                        <div className="row">
                            <div className="col-xs-2">
                                <a href="exchange.html" />
                                <div className="crypt-logo"><img src="images/logo.png" alt="" /></div>
                            </div>
                            <div className="col-xs-2">
                                <div className="crypt-mega-dropdown-menu"> <a href="" className="crypt-mega-dropdown-toggle">BTC/ETH <i className="pe-7s-angle-down-circle"></i></a>
                                    <div className="crypt-mega-dropdown-menu-block">
                                        <div className="crypt-market-status">
                                            <div>
                                                <div className="tab-content">
                                                    <div role="tabpanel" className="tab-pane active">
                                                        <table className="table table-striped">
                                                            <thead>
                                                                <tr>
                                                                    <th scope="col">Coin</th>
                                                                    <th scope="col">Price</th>
                                                                    <th scope="col">Volume</th>
                                                                    <th scope="col">Change</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <tr>
                                                                    <th scope="row">BTC</th>
                                                                    <td className="crypt-down">0.000056</td>
                                                                    <td>5.3424984</td>
                                                                    <td className="crypt-down"><b>-5.4%</b></td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">LTC</th>
                                                                    <td>0.0000564</td>
                                                                    <td>6.6768876</td>
                                                                    <td>-6.7%</td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">ETH</th>
                                                                    <td>0.0000234</td>
                                                                    <td>4.3456600</td>
                                                                    <td className="crypt-down">-5.6%</td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">XMR</th>
                                                                    <td>0.0000567</td>
                                                                    <td>4.3456600</td>
                                                                    <td>-5.6%</td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">DOGE</th>
                                                                    <td className="crypt-up">0.0000234</td>
                                                                    <td>4.3456600</td>
                                                                    <td className="crypt-up">-9.6%</td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">XMR</th>
                                                                    <td>0.0000567</td>
                                                                    <td>4.3456600</td>
                                                                    <td>-5.6%</td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                    <div role="tabpanel" className="tab-pane">
                                                        <table className="table table-striped">
                                                            <thead>
                                                                <tr>
                                                                    <th scope="col">Coin</th>
                                                                    <th scope="col">Price</th>
                                                                    <th scope="col">Volume</th>
                                                                    <th scope="col">Change</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <tr>
                                                                    <th scope="row">BTC</th>
                                                                    <td className="crypt-down">0.000056</td>
                                                                    <td>5.3424984</td>
                                                                    <td className="crypt-down"><b>-5.4%</b></td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">LTC</th>
                                                                    <td>0.0000564</td>
                                                                    <td>6.6768876</td>
                                                                    <td>-6.7%</td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">DOGE</th>
                                                                    <td className="crypt-up">0.0000234</td>
                                                                    <td>4.3456600</td>
                                                                    <td className="crypt-up">-9.6%</td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">XMR</th>
                                                                    <td>0.0000567</td>
                                                                    <td>4.3456600</td>
                                                                    <td>-5.6%</td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">DOGE</th>
                                                                    <td className="crypt-up">0.0000234</td>
                                                                    <td>4.3456600</td>
                                                                    <td className="crypt-up">-9.6%</td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">XMR</th>
                                                                    <td>0.0000567</td>
                                                                    <td>4.3456600</td>
                                                                    <td>-5.6%</td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                    <div role="tabpanel" className="tab-pane">
                                                        <table className="table table-striped">
                                                            <thead>
                                                                <tr>
                                                                    <th scope="col">Coin</th>
                                                                    <th scope="col">Price</th>
                                                                    <th scope="col">Volume</th>
                                                                    <th scope="col">Change</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <tr>
                                                                    <th scope="row">BTC</th>
                                                                    <td className="crypt-down">0.000056</td>
                                                                    <td>5.3424984</td>
                                                                    <td className="crypt-down"><b>-5.4%</b></td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">DOGE</th>
                                                                    <td className="crypt-up">0.0000234</td>
                                                                    <td>4.3456600</td>
                                                                    <td className="crypt-up">-9.6%</td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">XMR</th>
                                                                    <td>0.0000567</td>
                                                                    <td>4.3456600</td>
                                                                    <td>-5.6%</td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">DOGE</th>
                                                                    <td className="crypt-up">0.0000234</td>
                                                                    <td>4.3456600</td>
                                                                    <td className="crypt-up">-9.6%</td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">XMR</th>
                                                                    <td>0.0000567</td>
                                                                    <td>4.3456600</td>
                                                                    <td>-5.6%</td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="col-xl-8 col-lg-8 col-md-8 d-none d-md-flex d-lg-flex justify-content-end">
                        <ul className="crypt-heading-menu">
                            <li className="crypto-has-dropdown"><Link to="/">Exchange</Link></li>
                            <li><Link to="overview">Overview</Link></li>
                            <li><Link to="marketcap">Market Cap</Link></li>
                            <li><Link to="wallet">Wallet</Link></li>
                            <li className="crypt-box-menu menu-green mr-1"><Link to="/login">login</Link></li>
                            <li className="crypt-box-menu menu-red"><Link to="/register">register</Link></li>
                            
                        </ul>
                    </div><i className="menu-toggle pe-7s-menu d-xs-block d-sm-block d-md-none d-sm-none"></i></div>
            </div>
        </div>
        <div className="crypt-mobile-menu">
            <ul className="crypt-heading-menu">
                <li className="active"><a href="">Exchange</a></li>
                <li><a href="">Market Cap</a></li>
                <li><a href="">Treanding</a></li>
                <li><a href="">Tools</a></li>
                <li className="crypt-box-menu menu-green"><a href="">login</a></li>
                <li className="crypt-box-menu menu-red"><a href="">register</a></li>
                
            </ul>
            <div className="crypt-gross-market-cap">
                <h5>$34.795.90 <span className="crypt-up pl-2">+3.435 %</span></h5>
                <h6>0.7925.90 BTC <span className="crypt-down pl-2">+7.435 %</span></h6></div>
        </div>
    </header>
}

export default Header;