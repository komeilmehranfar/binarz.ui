import React from 'react';
import { Link, withRouter } from 'react-router-dom';
import Layout from '../../Components/Layout';

function Loading(){
    return(
        <div class="full-screen bg-white d-flex align-items-center">
            <div class="sp sp-circle"></div>
        </div>
    )
}
export default withRouter(Loading)