import React, {useState, useEffect} from 'react';
import empty from '../../assets/images/Empty.svg'
import Ticker from '../../Components/Ticker';
import { TradingBox } from '../../Components/TradingBox';
import { MarketTrading } from './Components/MarketTrading';
import { OrderBook } from './Components/OrderBook';
import Axios from 'axios';
import { withRouter } from 'react-router-dom';
import Loading from '../../Components/Loading';
function Exchange({history}){
    const [tab, setTab] = useState('usd');
    const [orderBookOrMarketTradingActive, setOrderBookOrMarketTradingActive] = useState('history')
    const [ordersTab, setOrdersTab] = useState('active-orders')
    const [tickerData, setTickerData] = useState(null)
    const [isLoading, setIsLoading] = useState(false)
    const tickerDataName = ['usd', 'btc', 'eth'];
    const getTicker = async () => {}

    useEffect(() => {
        const token = localStorage.getItem('token')
        if(!token){
          history.push('/login')
        }
    }, [])
    const renderApp = (
        <>
        <div className="container-fluid">
            <div className="row sm-gutters">
                <div className="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3 col-xxl-3">
                    <div className="crypt-market-status mt-4 mb-4">
                        <div>
                            <ul className="nav nav-tabs" id="crypt-tab">
                                {tickerDataName.map(ticker => 
                                    <li role="presentation" onClick={() => setTab(ticker)}><a className={tab === ticker ? 'active' : null}>{ticker}</a></li>
                                )}
                            </ul>
                        </div>
                        <div className="tab-content crypt-tab-content">
                            {tickerDataName.map(ticker => 
                                <div role="tabpanel" className={`tab-pane mb-4 ${tab === ticker ? 'active' : null}`} id={ticker}>
                                    <Ticker data={ticker}/>
                                </div>
                            )}
                        </div>
                    </div>
                </div>
                <div className="col-md-6 col-lg-6 col-xl-6 col-xxl-6">
                    <div className="crypt-gross-market-cap mt-4 mb-4">
                        <div className="row">
                            <div className="col-3 col-sm-6 col-md-6 col-lg-6">
                                <div className="row">
                                    <div className="col-sm-6 col-md-6 col-lg-6">
                                        <p>84568.85</p>
                                        <p>≈$8378.6850 USDT</p>
                                    </div>
                                    <div className="col-sm-6 col-md-6 col-lg-6">
                                        <p>24H Change</p>
                                        <p className="crypt-down">-0.0234230 -3.35%</p>
                                    </div>
                                </div>
                            </div>
                            <div className="col-3 col-sm-2 col-md-3 col-lg-2">
                                <p>24H High</p>
                                <p className="crypt-up">0.435453</p>
                            </div>
                            <div className="col-3 col-sm-2 col-md-3 col-lg-2">
                                <p>24H Low</p>
                                <p className="crypt-down">0.09945</p>
                            </div>
                            <div className="col-3 col-sm-2 col-md-3 col-lg-2">
                                <p>24H Volume</p>
                                <p>12.33445</p>
                            </div>
                        </div>
                    </div>
                    <div className="tradingview-widget-container mb-3">
                        <div id="crypt-candle-chart"></div>
                    </div>
                    {/* <div id="depthchart" className="depthchart h-40 crypt-dark-segment"></div> */}
                </div>
                <div className="col-md-3 col-lg-3 col-xl-3 col-xxl-3">
                    <div className="crypt-market-status mt-4 mb-4">
                        <div>
                            <ul className="nav nav-tabs">
                                <li role="presentation" onClick={() => setOrderBookOrMarketTradingActive('history')}><a className={orderBookOrMarketTradingActive === 'history' ? 'active' : null} data-toggle="tab">Order Book</a></li>
                                <li role="presentation" onClick={() => setOrderBookOrMarketTradingActive('market-trading')}><a className={orderBookOrMarketTradingActive === 'market-trading' ? 'active' : null} data-toggle="tab">market trading</a></li>
                            </ul>
                            <div className="tab-content">
                                <div role="tabpanel" className={`tab-pane ${orderBookOrMarketTradingActive === 'history' ? 'active' : null}`} id="history">
                                    <OrderBook/>
                                </div>
                                <div role="tabpanel" className={`tab-pane ${orderBookOrMarketTradingActive === 'market-trading' ? 'active' : null}`} id="market-trading">
                                    <MarketTrading/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div className="container-fluid">
            <div className="row sm-gutters">
                <div className="col-xl-5">
                    <TradingBox/>
                </div>
                <div className="col-xl-7">
                    <div>
                        <div className="crypt-market-status">
                            <div>
                                <ul className="nav nav-tabs">
                                    <li role="presentation" onClick={() => setOrdersTab('active-orders')}><a className={ordersTab === 'active-orders' ? 'active' : null} data-toggle="tab">Active Orders</a></li>
                                    <li role="presentation" onClick={() => setOrdersTab('closed-orders')}><a className={ordersTab === 'closed-orders' ? 'active' : null} data-toggle="tab">Closed Orders</a></li>
                                    <li role="presentation" onClick={() => setOrdersTab('balance')}><a className={ordersTab === 'balance' ? 'active' : null} data-toggle="tab">Balance</a></li>
                                </ul>
                                <div className="tab-content">
                                    <div role="tabpanel" className={`tab-pane ${ordersTab === 'active-orders' ? 'active' : null}`} id="active-orders">
                                        <table className="table table-striped">
                                            <thead>
                                                <tr>
                                                    <th scope="col">Time</th>
                                                    <th scope="col">Buy/sell</th>
                                                    <th scope="col">Price USDT</th>
                                                    <th scope="col">Amount BPS</th>
                                                    <th scope="col">Dealt BPS</th>
                                                    <th scope="col">Operation</th>
                                                </tr>
                                            </thead>
                                        </table>
                                        <div className="no-orders text-center p-160"><img src={empty} alt="no-orders" /></div>
                                    </div>
                                    <div role="tabpanel" className={`tab-pane ${ordersTab === 'closed-orders' ? 'active' : null}`} id="closed-orders">
                                        <table className="table table-striped">
                                            <thead>
                                                <tr>
                                                    <th scope="col">Time</th>
                                                    <th scope="col">Buy/sell</th>
                                                    <th scope="col">Price USDT</th>
                                                    <th scope="col">Amount BPS</th>
                                                    <th scope="col">Dealt BPS</th>
                                                    <th scope="col">Operation</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <th>22:35:59</th>
                                                    <td className="crypt-up">Buy</td>
                                                    <td className="crypt-up">0.000056</td>
                                                    <td className="crypt-up">0.000056</td>
                                                    <td className="crypt-up">0.0003456</td>
                                                    <td>5.3424984</td>
                                                </tr>
                                                <tr>
                                                    <th>22:35:59</th>
                                                    <td className="crypt-down">Sell</td>
                                                    <td className="crypt-down">0.000056</td>
                                                    <td className="crypt-down">0.000056</td>
                                                    <td className="crypt-down">0.0003456</td>
                                                    <td>5.3424984</td>
                                                </tr>
                                                <tr>
                                                    <th>22:35:59</th>
                                                    <td className="crypt-up">Buy</td>
                                                    <td className="crypt-up">0.000056</td>
                                                    <td className="crypt-up">0.000056</td>
                                                    <td className="crypt-up">0.0003456</td>
                                                    <td>5.3424984</td>
                                                </tr>
                                                <tr>
                                                    <th>22:35:59</th>
                                                    <td className="crypt-down">Sell</td>
                                                    <td className="crypt-down">0.000056</td>
                                                    <td className="crypt-down">0.000056</td>
                                                    <td className="crypt-down">0.0003456</td>
                                                    <td>5.3424984</td>
                                                </tr>
                                                <tr>
                                                    <th>22:35:59</th>
                                                    <td className="crypt-up">Buy</td>
                                                    <td className="crypt-up">0.000056</td>
                                                    <td className="crypt-up">0.000056</td>
                                                    <td className="crypt-up">0.0003456</td>
                                                    <td>5.3424984</td>
                                                </tr>
                                                <tr>
                                                    <th>22:35:59</th>
                                                    <td className="crypt-down">Sell</td>
                                                    <td className="crypt-down">0.000056</td>
                                                    <td className="crypt-down">0.000056</td>
                                                    <td className="crypt-down">0.0003456</td>
                                                    <td>5.3424984</td>
                                                </tr>
                                                <tr>
                                                    <th>22:35:59</th>
                                                    <td className="crypt-up">Buy</td>
                                                    <td className="crypt-up">0.000056</td>
                                                    <td className="crypt-up">0.000056</td>
                                                    <td className="crypt-up">0.0003456</td>
                                                    <td>5.3424984</td>
                                                </tr>
                                                <tr>
                                                    <th>22:35:59</th>
                                                    <td className="crypt-down">Sell</td>
                                                    <td className="crypt-down">0.000056</td>
                                                    <td className="crypt-down">0.000056</td>
                                                    <td className="crypt-down">0.0003456</td>
                                                    <td>5.3424984</td>
                                                </tr>
                                                <tr>
                                                    <th>22:35:59</th>
                                                    <td className="crypt-up">Buy</td>
                                                    <td className="crypt-up">0.000056</td>
                                                    <td className="crypt-up">0.000056</td>
                                                    <td className="crypt-up">0.0003456</td>
                                                    <td>5.3424984</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div role="tabpanel" className={`tab-pane ${ordersTab === 'balance' ? 'active' : null}`} id="balance">
                                        <table className="table table-striped">
                                            <thead>
                                                <tr>
                                                    <th scope="col">Currency</th>
                                                    <th scope="col">Amount</th>
                                                    <th scope="col">Volume</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <th>BTC</th>
                                                    <td>0.0000564</td>
                                                    <td>6.6768876</td>
                                                </tr>
                                                <tr>
                                                    <th>ETC</th>
                                                    <td>0.000056</td>
                                                    <td>5.3424984</td>
                                                </tr>
                                                <tr>
                                                    <th>LTC</th>
                                                    <td>0.0000234</td>
                                                    <td>4.3456600</td>
                                                </tr>
                                                <tr>
                                                    <th>XMR</th>
                                                    <td>0.0000234</td>
                                                    <td>4.3456600</td>
                                                </tr>
                                                <tr>
                                                    <th>BIT</th>
                                                    <td>0.0000567</td>
                                                    <td>4.3456600</td>
                                                </tr>
                                                <tr>
                                                    <th>EGF</th>
                                                    <td>0.0000234</td>
                                                    <td>4.3456600</td>
                                                </tr>
                                                <tr>
                                                    <th>EER</th>
                                                    <td>0.0000567</td>
                                                    <td>4.3456600</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </>
    )
    if(isLoading){
        return <Loading/>
    }
    return renderApp;
}

export default withRouter(Exchange);