import React from 'react';
export function OrderBook() {
    return (
        <>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th scope="col">Price</th>
                        <th scope="col">Quantity (BTC)</th>
                        <th scope="col">Total (BTC)</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td className="crypt-down">8700.80</td>
                        <td>0.0000234</td>
                        <td>4.3456600</td>
                    </tr>
                    <tr>
                        <td className="crypt-down">5410.56</td>
                        <td>0.0000234</td>
                        <td>4.3456600</td>
                    </tr>
                    <tr>
                        <td className="crypt-down">9800.43</td>
                        <td>0.0000567</td>
                        <td>4.3456600</td>
                    </tr>
                    <tr>
                        <td className="crypt-down">7090.78</td>
                        <td>0.0000234</td>
                        <td>4.3456600</td>
                    </tr>
                    <tr>
                        <td className="crypt-down">6577.88</td>
                        <td>0.0000567</td>
                        <td>4.3456600</td>
                    </tr>
                    <tr>
                        <td className="crypt-down">6556.34</td>
                        <td>0.0000564</td>
                        <td>6.6768876</td>
                    </tr>
                    <tr>
                        <td className="crypt-down">8887.70</td>
                        <td>0.000056</td>
                        <td>5.3424984</td>
                    </tr>
                    <tr>
                        <td className="crypt-down">6577.87</td>
                        <td>0.0000564</td>
                        <td>6.6768876</td>
                    </tr>
                    <tr>
                        <td className="crypt-down">7324.44</td>
                        <td>0.0000234</td>
                        <td>4.3456600</td>
                    </tr>
                    <tr>
                        <td className="crypt-down">7111.56</td>
                        <td>0.0000234</td>
                        <td>4.3456600</td>
                    </tr>
                    <tr>
                        <td className="crypt-down">5888.98</td>
                        <td>0.0000567</td>
                        <td>4.3456600</td>
                    </tr>
                    <tr>
                        <td className="crypt-down">6590.08</td>
                        <td>0.0000234</td>
                        <td>4.3456600</td>
                    </tr>
                </tbody>
            </table>
            <h6 className="text-center pt-2 pt-2">29384798 <span className="pl-3">938475</span></h6>
            <table className="table table-striped">
                <tbody>
                    <tr>
                        <td className="crypt-up">4300.67</td>
                        <td>0.0000567</td>
                        <td>4.3456600</td>
                    </tr>
                    <tr>
                        <td className="crypt-up">9510.56</td>
                        <td>0.0000564</td>
                        <td>6.6768876</td>
                    </tr>
                    <tr>
                        <td className="crypt-up">9080.67</td>
                        <td>0.000056</td>
                        <td>5.3424984</td>
                    </tr>
                    <tr>
                        <td className="crypt-up">2346.64</td>
                        <td>0.0000564</td>
                        <td>6.6768876</td>
                    </tr>
                    <tr>
                        <td className="crypt-up">5478.87</td>
                        <td>0.0000234</td>
                        <td>4.3456600</td>
                    </tr>
                    <tr>
                        <td className="crypt-up">5689.78</td>
                        <td>0.0000234</td>
                        <td>4.3456600</td>
                    </tr>
                    <tr>
                        <td className="crypt-up">4518.56</td>
                        <td>0.0000567</td>
                        <td>4.3456600</td>
                    </tr>
                    <tr>
                        <td className="crypt-up">6900.67</td>
                        <td>0.0000234</td>
                        <td>4.3456600</td>
                    </tr>
                    <tr>
                        <td className="crypt-up">6898.56</td>
                        <td>0.0000567</td>
                        <td>4.3456600</td>
                    </tr>
                    <tr>
                        <td className="crypt-up">7894.34</td>
                        <td>0.0000564</td>
                        <td>6.6768876</td>
                    </tr>
                    <tr>
                        <td className="crypt-up">8765.90</td>
                        <td>0.0000234</td>
                        <td>4.3456600</td>
                    </tr>
                    <tr>
                        <td className="crypt-up">5674.76</td>
                        <td>0.0000234</td>
                        <td>4.3456600</td>
                    </tr>
                    <tr>
                        <td className="crypt-up">3452.09</td>
                        <td>0.0000567</td>
                        <td>4.3456600</td>
                    </tr>
                    <tr>
                        <td className="crypt-up">7689.65</td>
                        <td>0.0000234</td>
                        <td>4.3456600</td>
                    </tr>
                    <tr>
                        <td className="crypt-up">3468.34</td>
                        <td>0.0000567</td>
                        <td>4.3456600</td>
                    </tr>
                    <tr>
                        <td className="crypt-up">8794.12</td>
                        <td>0.0000564</td>
                        <td>6.6768876</td>
                    </tr>
                    <tr>
                        <td className="crypt-up">2315.86</td>
                        <td>0.0000234</td>
                        <td>4.3456600</td>
                    </tr>
                    <tr>
                        <td className="crypt-up">463.98</td>
                        <td>0.0000234</td>
                        <td>4.3456600</td>
                    </tr>
                    <tr>
                        <td className="crypt-up">673.67</td>
                        <td>0.0000567</td>
                        <td>4.3456600</td>
                    </tr>
                </tbody>
            </table>
        </>
    )
}