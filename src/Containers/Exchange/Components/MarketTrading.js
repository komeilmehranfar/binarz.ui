import React from 'react';
export function MarketTrading() {
    return (
        <table className="table table-striped">
            <thead>
                <tr>
                    <th scope="col">Time</th>
                    <th scope="col">Amount</th>
                    <th scope="col">Volume</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>22:35:59</td>
                    <td>0.0000564</td>
                    <td>6.6768876</td>
                </tr>
                <tr>
                    <td>22:35:59</td>
                    <td className="crypt-down">0.000056</td>
                    <td>5.3424984</td>
                </tr>
                <tr>
                    <td>22:35:59</td>
                    <td className="crypt-up">0.0000234</td>
                    <td>4.3456600</td>
                </tr>
                <tr>
                    <td>22:35:59</td>
                    <td>0.0000234</td>
                    <td>4.3456600</td>
                </tr>
                <tr>
                    <td>22:35:59</td>
                    <td>0.0000567</td>
                    <td>4.3456600</td>
                </tr>
                <tr>
                    <td>22:35:59</td>
                    <td className="crypt-up">0.0000234</td>
                    <td>4.3456600</td>
                </tr>
                <tr>
                    <td>22:35:59</td>
                    <td>0.0000567</td>
                    <td>4.3456600</td>
                </tr>
                <tr>
                    <td>22:35:59</td>
                    <td>0.0000564</td>
                    <td>6.6768876</td>
                </tr>
                <tr>
                    <td>22:35:59</td>
                    <td className="crypt-down">0.000056</td>
                    <td>5.3424984</td>
                </tr>
                <tr>
                    <td>22:35:59</td>
                    <td className="crypt-up">0.0000234</td>
                    <td>4.3456600</td>
                </tr>
                <tr>
                    <td>22:35:59</td>
                    <td>0.0000234</td>
                    <td>4.3456600</td>
                </tr>
                <tr>
                    <td>22:35:59</td>
                    <td>0.0000567</td>
                    <td>4.3456600</td>
                </tr>
                <tr>
                    <td>22:35:59</td>
                    <td className="crypt-up">0.0000234</td>
                    <td>4.3456600</td>
                </tr>
                <tr>
                    <td>22:35:59</td>
                    <td>0.0000567</td>
                    <td>4.3456600</td>
                </tr>
                <tr>
                    <td>22:35:59</td>
                    <td>0.0000564</td>
                    <td>6.6768876</td>
                </tr>
                <tr>
                    <td>22:35:59</td>
                    <td className="crypt-down">0.000056</td>
                    <td>5.3424984</td>
                </tr>
                <tr>
                    <td>22:35:59</td>
                    <td className="crypt-up">0.0000234</td>
                    <td>4.3456600</td>
                </tr>
                <tr>
                    <td>22:35:59</td>
                    <td>0.0000234</td>
                    <td>4.3456600</td>
                </tr>
                <tr>
                    <td>22:35:59</td>
                    <td>0.0000567</td>
                    <td>4.3456600</td>
                </tr>
                <tr>
                    <td>22:35:59</td>
                    <td className="crypt-up">0.0000234</td>
                    <td>4.3456600</td>
                </tr>
            </tbody>
        </table>
    )
}