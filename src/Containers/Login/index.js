import React, { useRef, useState } from 'react';
import { Link, withRouter } from 'react-router-dom';
import logo from '../../assets/images/logo.png'
import Layout from '../../Components/Layout';
import Axios from 'axios';
import Loading from '../../Components/Loading';
function Login({history}){
    const [isLoading, setLoading] = useState(false)
    const userName = useRef(null)
    const pass = useRef(null)
    const openAmLogin = (e) => {
        setLoading(true)
        e.preventDefault()
        const data = new FormData();
        data.append('grant_type', 'password');
        data.append('client_id', 'binarz');
        data.append('scope', 'openid');
        data.append('username', userName.current.value);
        data.append('password', pass.current.value);
        setTimeout(() => {
            setLoading(false)
            history.push('/')
            localStorage.setItem('token', 'value')
            Axios.defaults.headers.common['token'] = 'value' // for all requests
        }, 1000);
        // Axios.post('http://binarz.com:18080/sopenam/oauth2/access_token', data)
    }
    const renderApp = (
        <Layout>
            <div className="container">
                <div className="row">
                    <div className="col-md-3"></div>
                    <div className="col-md-6">
                        <div className="cryptorio-forms cryptorio-forms-dark text-center pt-5 pb-5">
                            <div className="logo">
                                <img src={logo} alt="logo"/>
                            </div>
                            <h3 className="p-4">Welcome To Login</h3>
                            <div className="cryptorio-main-form">
                                <form action="" className="text-left">
                                    <label for="email">Account Name</label>
                                    <input ref={userName} type="text" id="email" name="email" placeholder="Your email/cellphone"/>
                                    <label for="password">Password</label>
                                    <input ref={pass} type="password" id="password" name="password" placeholder="Please Input Your Password"/>

                                    <button className="crypt-button-red-full" onClick={openAmLogin}>
                                        login
                                    </button>
                                </form>
                                <p className="float-left"><Link to="register">Sign Up</Link></p>
                            </div>
                        </div>
                    </div>
                    <div className="col-md-3"></div>
                </div>
            </div>
        </Layout>
    )
    if(isLoading){
        return <Loading/>
    }
    return(
        renderApp
    )
}
export default withRouter(Login)