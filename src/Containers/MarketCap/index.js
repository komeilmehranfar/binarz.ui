import React, { useEffect } from 'react';
import arrow from '../../assets/images/download-arrow.svg';
import btc from '../../assets/images/coins/btc.png';
import eth from '../../assets/images/coins/eth.png';
import xrp from '../../assets/images/coins/xrp.png';
import zc from '../../assets/images/coins/zc.png';
import btcc from '../../assets/images/coins/btcc.png';
import ethc from '../../assets/images/coins/ethc.png';
import tether from '../../assets/images/coins/tether.png';
import ltc from '../../assets/images/coins/ltc.png';
import monero from '../../assets/images/coins/monero.png';
import tron from '../../assets/images/coins/tron.png';
import neo from '../../assets/images/coins/neo.png';
import nem from '../../assets/images/coins/nem.png';
import cardano from '../../assets/images/coins/cardano.png';
import doge from '../../assets/images/coins/doge.png';
import zil from '../../assets/images/coins/zil.png';
import bite from '../../assets/images/coins/bite.png';
import upArrow from '../../assets/images/up-arrow.svg'
import { withRouter } from 'react-router-dom';

function MarketCap({history}){
	useEffect(() => {
        const token = localStorage.getItem('token')
        if(!token){
          history.push('/login')
        }
    }, [])
    return (
        <div class="container">
		<div class="row sm-gutters">
			<div class="col-md-6 col-lg-4 col-xl-3">
				<div class="crypt-fulldiv-linechart mt-4">
					<h4 class="coinname"><img src={btc} width="20" class="crypt-market-cap-logo" alt="logo"/> Bitcoin</h4>
					<div
		      			class ="crypt-individual-marketcap"
		      			data-charts ="[65,59,81,81,56,55,40,80,90]" 
		      			data-bg = "rgba(247,97,78,0.5)"
		      			data-border = "f7614e">
			      		<canvas></canvas>
			      	</div>
			      	<div class="coin-meta-data text-center">
			      		<h5 class="crypt-down">-9.05% <img src={arrow} width="15" class="crypt-market-cap-logo" alt="logo"/></h5>
			      		<h4>$4,056,894.05</h4>
			      		<p><b>MARKET CAP</b></p>
			      	</div>
				</div>
			</div>
			<div class="col-md-6 col-lg-4 col-xl-3">
				<div class="crypt-fulldiv-linechart mt-4">
					<h4 class="coinname"><img src={eth} width="20" class="crypt-market-cap-logo" alt="logo"/> Ethrium</h4>
					<div
		      			class ="crypt-individual-marketcap"
		      			data-charts ="[65,59,81,81,56,55,40,80,90]" 
		      			data-bg = "rgba(73,194,121,0.5)"
		      			data-border = "49c279">
			      		<canvas></canvas>
			      	</div>
			      	<div class="coin-meta-data text-center">
			      		<h5 class="crypt-up">+4.25% <img src={upArrow} width="15" class="crypt-market-cap-logo" alt="logo"/></h5>
			      		<h4>$4,056,894.05</h4>
			      		<p><b>MARKET CAP</b></p>
			      	</div>
				</div>
			</div>
			<div class="col-md-6 col-lg-4 col-xl-3">
				<div class="crypt-fulldiv-linechart mt-4">
					<h4 class="coinname"><img src={xrp} width="20" class="crypt-market-cap-logo" alt="logo"/>  XRP</h4>
					<div
		      			class ="crypt-individual-marketcap"
		      			data-charts ="[55,69,41,61,56,54,44,50,60]" 
		      			data-bg = "rgba(73,194,121,0.5)"
		      			data-border = "49c279">
			      		<canvas></canvas>
			      	</div>
			      	<div class="coin-meta-data text-center">
			      		<h5 class="crypt-up">+2.25% <img src={upArrow} width="15" class="crypt-market-cap-logo" alt="logo"/></h5>
			      		<h4>$4,056,894.05</h4>
			      		<p><b>MARKET CAP</b></p>
			      	</div>
				</div>
			</div>
			<div class="col-md-6 col-lg-4 col-xl-3">
				<div class="crypt-fulldiv-linechart mt-4">
					<h4 class="coinname"><img src={btcc} width="20" class="crypt-market-cap-logo" alt="logo"/> BTCC</h4>
					<div
		      			class ="crypt-individual-marketcap"
		      			data-charts ="[15,49,41,61,76,84,44,60,78]" 
		      			data-bg = "rgba(247,97,78,0.5)"
		      			data-border = "f7614e">
			      		<canvas></canvas>
			      	</div>
			      	<div class="coin-meta-data text-center">
			      		<h5 class="crypt-down">-4.05% <img src={arrow} width="15" class="crypt-market-cap-logo" alt="logo"/></h5>
			      		<h4>$4,056,894.05</h4>
			      		<p><b>MARKET CAP</b></p>
			      	</div>
				</div>
			</div>
			<div class="col-md-6 col-lg-4 col-xl-3">
				<div class="crypt-fulldiv-linechart mt-4">
					<h4 class="coinname"><img src={ltc} width="20" class="crypt-market-cap-logo" alt="logo"/> LTC</h4>
					<div
		      			class ="crypt-individual-marketcap"
		      			data-charts ="[45,37,41,41,66,54,44,60,78]" 
		      			data-bg = "rgba(247,97,78,0.5)"
		      			data-border = "f7614e">
			      		<canvas></canvas>
			      	</div>
			      	<div class="coin-meta-data text-center">
			      		<h5 class="crypt-down">-12.05% <img src={arrow} width="15" class="crypt-market-cap-logo" alt="logo"/></h5>
			      		<h4>$4,056,894.05</h4>
			      		<p><b>MARKET CAP</b></p>
			      	</div>
				</div>
			</div>
			<div class="col-md-6 col-lg-4 col-xl-3">
				<div class="crypt-fulldiv-linechart mt-4">
					<h4 class="coinname"><img src={tether} width="20" class="crypt-market-cap-logo" alt="logo"/> Tether</h4>
					<div
		      			class ="crypt-individual-marketcap"
		      			data-charts ="[55,69,41,61,56,54,44,50,60]" 
		      			data-bg = "rgba(73,194,121,0.5)"
		      			data-border = "49c279">
			      		<canvas></canvas>
			      	</div>
			      	<div class="coin-meta-data text-center">
			      		<h5 class="crypt-up">+6.67% <img src={upArrow} width="15" class="crypt-market-cap-logo" alt="logo"/></h5>
			      		<h4>$4,056,894.05</h4>
			      		<p><b>MARKET CAP</b></p>
			      	</div>
				</div>
			</div>
			<div class="col-md-6 col-lg-4 col-xl-3">
				<div class="crypt-fulldiv-linechart mt-4">
					<h4 class="coinname"><img src={monero} width="20" class="crypt-market-cap-logo" alt="logo"/> Monero</h4>
					<div
		      			class ="crypt-individual-marketcap"
		      			data-charts ="[45,37,41,41,66,54,44,60,78]" 
		      			data-bg = "rgba(247,97,78,0.5)"
		      			data-border = "f7614e">
			      		<canvas></canvas>
			      	</div>
			      	<div class="coin-meta-data text-center">
			      		<h5 class="crypt-down">-3.45% <img src={arrow} width="15" class="crypt-market-cap-logo" alt="logo"/></h5>
			      		<h4>$4,056,894.05</h4>
			      		<p><b>MARKET CAP</b></p>
			      	</div>
				</div>
			</div>
			<div class="col-md-6 col-lg-4 col-xl-3">
				<div class="crypt-fulldiv-linechart mt-4">
					<h4 class="coinname"><img src={tron} width="20" class="crypt-market-cap-logo" alt="logo"/> Tron</h4>
					<div
		      			class ="crypt-individual-marketcap"
		      			data-charts ="[65,59,81,81,56,55,40,80,90]" 
		      			data-bg = "rgba(73,194,121,0.5)"
		      			data-border = "49c279">
			      		<canvas></canvas>
			      	</div>
			      	<div class="coin-meta-data text-center">
			      		<h5 class="crypt-up">+8.05% <img src={upArrow} width="15" class="crypt-market-cap-logo" alt="logo"/></h5>
			      		<h4>$4,056,894.05</h4>
			      		<p><b>MARKET CAP</b></p>
			      	</div>
				</div>
			</div>
			<div class="col-md-6 col-lg-4 col-xl-3">
				<div class="crypt-fulldiv-linechart mt-4">
					<h4 class="coinname"><img src={neo} width="20" class="crypt-market-cap-logo" alt="logo"/> NEO</h4>
					<div
		      			class ="crypt-individual-marketcap"
		      			data-charts ="[65,59,81,81,56,55,40,80,90]" 
		      			data-bg = "rgba(247,97,78,0.5)"
		      			data-border = "f7614e">
			      		<canvas></canvas>
			      	</div>
			      	<div class="coin-meta-data text-center">
			      		<h5 class="crypt-down">-9.05% <img src={arrow} width="15" class="crypt-market-cap-logo" alt="logo"/></h5>
			      		<h4>$4,056,894.05</h4>
			      		<p><b>MARKET CAP</b></p>
			      	</div>
				</div>
			</div>
			<div class="col-md-6 col-lg-4 col-xl-3">
				<div class="crypt-fulldiv-linechart mt-4">
					<h4 class="coinname"><img src={ethc} width="20" class="crypt-market-cap-logo" alt="logo"/> ETH CLassic</h4>
					<div
		      			class ="crypt-individual-marketcap"
		      			data-charts ="[65,59,81,81,56,55,40,80,90]" 
		      			data-bg = "rgba(73,194,121,0.5)"
		      			data-border = "49c279">
			      		<canvas></canvas>
			      	</div>
			      	<div class="coin-meta-data text-center">
			      		<h5 class="crypt-up">+4.25% <img src={upArrow} width="15" class="crypt-market-cap-logo" alt="logo"/></h5>
			      		<h4>$4,056,894.05</h4>
			      		<p><b>MARKET CAP</b></p>
			      	</div>
				</div>
			</div>
			<div class="col-md-6 col-lg-4 col-xl-3">
				<div class="crypt-fulldiv-linechart mt-4">
					<h4 class="coinname"><img src={zc} width="20" class="crypt-market-cap-logo" alt="logo"/>  Zcash</h4>
					<div
		      			class ="crypt-individual-marketcap"
		      			data-charts ="[55,69,41,61,56,54,44,50,60]" 
		      			data-bg = "rgba(73,194,121,0.5)"
		      			data-border = "49c279">
			      		<canvas></canvas>
			      	</div>
			      	<div class="coin-meta-data text-center">
			      		<h5 class="crypt-up">+2.25% <img src={upArrow} width="15" class="crypt-market-cap-logo" alt="logo"/></h5>
			      		<h4>$4,056,894.05</h4>
			      		<p><b>MARKET CAP</b></p>
			      	</div>
				</div>
			</div>
			<div class="col-md-6 col-lg-4 col-xl-3">
				<div class="crypt-fulldiv-linechart mt-4">
					<h4 class="coinname"><img src={nem} width="20" class="crypt-market-cap-logo" alt="logo"/> NEM</h4>
					<div
		      			class ="crypt-individual-marketcap"
		      			data-charts ="[15,49,41,61,56,44,76,45,80,76,84,44,60,78]" 
		      			data-bg = "rgba(247,97,78,0.5)"
		      			data-border = "f7614e">
			      		<canvas></canvas>
			      	</div>
			      	<div class="coin-meta-data text-center">
			      		<h5 class="crypt-down">-4.05% <img src={arrow} width="15" class="crypt-market-cap-logo" alt="logo"/></h5>
			      		<h4>$4,056,894.05</h4>
			      		<p><b>MARKET CAP</b></p>
			      	</div>
				</div>
			</div>
			<div class="col-md-6 col-lg-4 col-xl-3">
				<div class="crypt-fulldiv-linechart mt-4">
					<h4 class="coinname"><img src={cardano} width="20" class="crypt-market-cap-logo" alt="logo"/> Cardano</h4>
					<div
		      			class ="crypt-individual-marketcap"
		      			data-charts ="[45,37,41,41,66,54,44,60,78]" 
		      			data-bg = "rgba(247,97,78,0.5)"
		      			data-border = "f7614e">
			      		<canvas></canvas>
			      	</div>
			      	<div class="coin-meta-data text-center">
			      		<h5 class="crypt-down">-12.05% <img src={arrow} width="15" class="crypt-market-cap-logo" alt="logo"/></h5>
			      		<h4>$4,056,894.05</h4>
			      		<p><b>MARKET CAP</b></p>
			      	</div>
				</div>
			</div>
			<div class="col-md-6 col-lg-4 col-xl-3">
				<div class="crypt-fulldiv-linechart mt-4">
					<h4 class="coinname"><img src={doge} width="20" class="crypt-market-cap-logo" alt="logo"/> Doge</h4>
					<div
		      			class ="crypt-individual-marketcap"
		      			data-charts ="[15,49,41,61,76,84,44,60,78]" 
		      			data-bg = "rgba(73,194,121,0.5)"
		      			data-border = "49c279">
			      		<canvas></canvas>
			      	</div>
			      	<div class="coin-meta-data text-center">
			      		<h5 class="crypt-up">+6.67% <img src={upArrow} width="15" class="crypt-market-cap-logo" alt="logo"/></h5>
			      		<h4>$4,056,894.05</h4>
			      		<p><b>MARKET CAP</b></p>
			      	</div>
				</div>
			</div>
			<div class="col-md-6 col-lg-4 col-xl-3">
				<div class="crypt-fulldiv-linechart mt-4">
					<h4 class="coinname"><img src={zil} width="20" class="crypt-market-cap-logo" alt="logo"/> Ziliqua</h4>
					<div
		      			class ="crypt-individual-marketcap"
		      			data-charts ="[55,69,41,61,56,54,44,50,60]" 
		      			data-bg = "rgba(247,97,78,0.5)"
		      			data-border = "f7614e">
			      		<canvas></canvas>
			      	</div>
			      	<div class="coin-meta-data text-center">
			      		<h5 class="crypt-down">-3.45% <img src={arrow} width="15" class="crypt-market-cap-logo" alt="logo"/></h5>
			      		<h4>$4,056,894.05</h4>
			      		<p><b>MARKET CAP</b></p>
			      	</div>
				</div>
			</div>
			<div class="col-md-6 col-lg-4 col-xl-3">
				<div class="crypt-fulldiv-linechart mt-4">
					<h4 class="coinname"><img src={bite} width="20" class="crypt-market-cap-logo" alt="logo"/> Bitecoin</h4>
					<div
		      			class ="crypt-individual-marketcap"
		      			data-charts ="[65,59,81,81,56,55,40,80,90]" 
		      			data-bg = "rgba(73,194,121,0.5)"
		      			data-border = "49c279">
			      		<canvas></canvas>
			      	</div>
			      	<div class="coin-meta-data text-center">
			      		<h5 class="crypt-up">+8.05% <img src={upArrow} width="15" class="crypt-market-cap-logo" alt="logo"/></h5>
			      		<h4>$4,056,894.05</h4>
			      		<p><b>MARKET CAP</b></p>
			      	</div>
				</div>
			</div>
		</div>
	</div>
    )
}

export default withRouter(MarketCap)