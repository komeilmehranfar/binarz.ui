import React from 'react';
import { Link, withRouter } from 'react-router-dom';
import logo from '../../assets/images/logo.png'
import Layout from '../../Components/Layout';

function Register(){
    return(
        <Layout>
            <div class="container">
                <div class="row">
                    <div class="col-md-3"></div>
                    <div class="col-md-6">
                        <div class="cryptorio-forms cryptorio-forms-dark text-center pt-5 pb-5">
                            <div class="logo">
                                <img src={logo} alt="logo"/>
                            </div>
                            <h3 class="p-4">Register</h3>
                            <div class="cryptorio-main-form">
                                <form action="" class="text-left">
                                    <label for="email">Email</label>
                                    <input type="text" id="email" name="email" placeholder="Your email/cellphone"/>
                                    <label for="password">Password</label>
                                    <input type="password" id="password" name="password" placeholder="6-20 letters and numbers"/>
                                    <label for="confirm-password">Confirm Password</label>
                                    <input type="password" id="confirm-password" name="password" placeholder="6-20 letters and numbers"/>
                                    <div class="my-1">
                                    <div class="custom-control custom-checkbox mr-sm-2">
                                        <input type="checkbox" class="custom-control-input" id="terms-agree"/>
                                        <label class="custom-control-label" for="terms-agree">I agree to the terms of services</label>
                                    </div>
                                    </div>
                                    <input type="submit" value="SignUp" class="crypt-button-red-full"/>
                                </form>
                                <p class="float-left"><Link to="login">Log In</Link></p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3"></div>
                </div>
            </div>
        </Layout>
    )
}
export default withRouter(Register)