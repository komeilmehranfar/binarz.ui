import React, { useEffect } from 'react';
import btc from '../../assets/images/coins/btc.png';
import eth from '../../assets/images/coins/eth.png';
import xrp from '../../assets/images/coins/xrp.png';
import btcc from '../../assets/images/coins/btcc.png';
import tether from '../../assets/images/coins/tether.png';
import ltc from '../../assets/images/coins/ltc.png';
import monero from '../../assets/images/coins/monero.png';
import tron from '../../assets/images/coins/tron.png';
import dash from '../../assets/images/coins/dash.png';
import stlr from '../../assets/images/coins/stlr.png';
import { withRouter } from 'react-router-dom';
function Overview({history}){
    useEffect(() => {
        const token = localStorage.getItem('token')
        if(!token){
          history.push('/login')
        }
    }, [])
    return (
        <>
            <div class="container">
                <div class="row">
                    <div class="col md-12 text-center">
                        <h1 class="mt-5 mb-5 text-white">Top 100 Cryptocurrencies By Market Capitalization</h1>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="table-responsive">
                            <table class="table table-striped crypt-table-market-cap">
                            <thead>
                                <tr>
                                <th scope="col">Rank</th>
                                <th scope="col" class="text-left pl-2">Name</th>
                                <th scope="col">Market Cap</th>
                                <th scope="col">Price</th>
                                <th scope="col">24 Hour Volume</th>
                                <th scope="col">Change</th>
                                <th scope="col">7 days</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                <th>1</th>
                                <td class="text-left pl-2 font-weight-bold"><img src={btc} width="20" class="pr-1 crypt-market-cap-logo" alt="coin"/> <a href="exchange.html">Bitcoin</a></td>
                                <td>$239048209</td>
                                <td>$4559</td>
                                <td>$2344234</td>
                                <td class="crypt-down"> -0.04%</td>
                                <td
                                    class ="crypt-marketcap-canvas"
                                    data-charts ="[65,59,81,81,56,55,40,80,90]" 
                                    data-bg = "rgba(247,97,78,0.5)"
                                    data-border = "f7614e">
                                    <canvas></canvas>
                                </td>
                                </tr>
                                <tr>
                                <th>2</th>
                                <td class="text-left pl-2 font-weight-bold"><img src={eth} width="20" class="pr-1 crypt-market-cap-logo" alt="coin"/> <a href="exchange.html">Ethrium</a></td>
                                <td>$239048209</td>
                                <td>$4559</td>
                                <td>$2344234</td>
                                <td class="crypt-down"> -4.04%</td>
                                <td
                                    class ="crypt-marketcap-canvas"
                                    data-charts ="[34,54,23,56,76,78,34,56,45]" 
                                    data-bg = "rgba(247,97,78,0.5)"
                                    data-border = "f7614e">
                                    <canvas></canvas>
                                </td>
                                </tr>
                                <tr>
                                <th>3</th>
                                <td class="text-left pl-2 font-weight-bold"><img src={ltc} width="20" class="pr-1 crypt-market-cap-logo" alt="coin"/> <a href="exchange.html">Litecoin</a></td>
                                <td>$239048209</td>
                                <td>$4559</td>
                                <td>$2344234</td>
                                <td class="crypt-up">3.04%</td>
                                <td
                                    class ="crypt-marketcap-canvas"
                                    data-charts ="[55,40,80,90,65,59,81,81,56]" 
                                    data-bg = "rgba(73,194,121,0.5)"
                                    data-border = "49c279">
                                    <canvas></canvas>
                                </td>
                                </tr>
                                <tr>
                                <th>4</th>
                                <td class="text-left pl-2 font-weight-bold"><img src={btcc} width="20" class="pr-1 crypt-market-cap-logo" alt="coin"/>  <a href="exchange.html">
                                    BTCC
                                </a></td>
                                <td>$239048209</td>
                                <td>$4559</td>
                                <td>$2344234</td>
                                <td class="crypt-down">-0.45%</td>
                                <td
                                    class ="crypt-marketcap-canvas"
                                    data-charts ="[89,23,67,45,23,45,78,34,56,67]" 
                                    data-bg = "rgba(247,97,78,0.5)"
                                    data-border = "f7614e">
                                    <canvas></canvas>
                                </td>
                                </tr>
                                <tr>
                                <th>5</th>
                                <td class="text-left pl-2 font-weight-bold"><img src={xrp} width="20" class="pr-1 crypt-market-cap-logo" alt="coin"/> <a href="exchange.html">
                                    XRP
                                </a></td>
                                <td>$239048209</td>
                                <td>$4559</td>
                                <td>$2344234</td>
                                <td class="crypt-up">6.04%</td>
                                <td
                                    class ="crypt-marketcap-canvas"
                                    data-charts ="[34,45,56,67,65,45,34,23,45,56]" 
                                    data-bg = "rgba(73,194,121,0.5)"
                                    data-border = "49c279">
                                    <canvas></canvas>
                                </td>
                                </tr>
                                <tr>
                                <th>6</th>
                                <td class="text-left pl-2 font-weight-bold"><img src={tron} width="20" class="pr-1 crypt-market-cap-logo" alt="coin"/> <a href="exchange.html">
                                    TRON
                                </a></td>
                                <td>$239048209</td>
                                <td>$4559</td>
                                <td>$2344234</td>
                                <td class="crypt-down">-6.7%</td>
                                <td
                                    class ="crypt-marketcap-canvas"
                                    data-charts ="[45,34,45,56,67,45,56,67,78,67]" 
                                    data-bg = "rgba(247,97,78,0.5)"
                                    data-border = "f7614e">
                                    <canvas></canvas>
                                </td>
                                </tr>
                                <tr>
                                <th>7</th>
                                <td class="text-left pl-2 font-weight-bold"><img src={stlr} width="20" class="pr-1 crypt-market-cap-logo" alt="coin"/> <a href="exchange.html">
                                    Steller
                                </a></td>
                                <td>$239048209</td>
                                <td>$4559</td>
                                <td>$2344234</td>
                                <td class="crypt-up">2.04%</td>
                                <td
                                    class ="crypt-marketcap-canvas"
                                    data-charts ="[56,45,67,45,34,45,56,67,56,45]" 
                                    data-bg = "rgba(73,194,121,0.5)"
                                    data-border = "49c279">
                                    <canvas></canvas>
                                </td>
                                </tr>
                                <tr>
                                <th>8</th>
                                <td class="text-left pl-2 font-weight-bold"><img src={tether} width="20" class="pr-1 crypt-market-cap-logo" alt="coin"/> <a href="exchange.html">
                                    Tether
                                </a></td>
                                <td>$239048209</td>
                                <td>$4559</td>
                                <td>$2344234</td>
                                <td class="crypt-up">5.04%</td>
                                <td
                                    class ="crypt-marketcap-canvas"
                                    data-charts ="[65,59,81,81,56,55,40,80,90]" 
                                    data-bg = "rgba(73,194,121,0.5)"
                                    data-border = "49c279">
                                    <canvas></canvas>
                                </td>
                                </tr>
                                <tr>
                                <th>9</th>
                                <td class="text-left pl-2 font-weight-bold"><img src={dash} width="20" class="pr-1 crypt-market-cap-logo" alt="coin"/> <a href="exchange.html">
                                    Dash
                                </a></td>
                                <td>$239048209</td>
                                <td>$4559</td>
                                <td>$2344234</td>
                                <td class="crypt-up">2.04%</td>
                                <td
                                    class ="crypt-marketcap-canvas"
                                    data-charts ="[65,59,81,81,56,55,40,80,90]" 
                                    data-bg = "rgba(73,194,121,0.5)"
                                    data-border = "49c279">
                                    <canvas></canvas>
                                </td>
                                </tr>
                                <tr>
                                <th>9</th>
                                <td class="text-left pl-2 font-weight-bold"><img src={monero} width="20" class="pr-1 crypt-market-cap-logo" alt="coin"/> <a href="exchange.html">
                                    Monero
                                </a></td>
                                <td>$239048209</td>
                                <td>$4559</td>
                                <td>$2344234</td>
                                <td class="crypt-up">3.04%</td>
                                <td
                                    class ="crypt-marketcap-canvas"
                                    data-charts ="[65,81,40,80,56,59,81,55,90]" 
                                    data-bg = "rgba(73,194,121,0.5)"
                                    data-border = "49c279">
                                    <canvas></canvas>
                                </td>
                                </tr>
                            </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="row mb-4 mt-4">
                    <div class="col-md-6">
                        <a href="" class="crypt-btn crypt-button-inline crypt-btn-green">Next 100 <i class="pe pe-7s-angle-right-circle"></i></a>
                        <a href="" class="crypt-btn crypt-button-inline crypt-btn-red">View All</a>
                    </div>
                    <div class="col-md-6 mt-3">
                        <p><i>*Coin Market Cap provides direct cryptocurrency market capitalization data</i></p>
                    </div>
                </div>
                <hr />
                <div class="row">
                    <div class="col-md-12">
                        <div class="text-center">
                            <h1 class="mt-5 mb-3"><b>Total Market Cap: <span class="crypt-up">$884,056,234,480</span></b></h1>
                            <p class="mb-5">Last updated: Nov 20, 2019 3:00 PM UTC</p>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}
export default withRouter(Overview)