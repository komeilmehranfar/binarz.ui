import React, {useEffect} from 'react';
import {
  Switch,
  Route,
  withRouter,
} from "react-router-dom";
import Exchange from './Containers/Exchange';
import './assets/styles/style.scss'
// import './assets/styles/style-light.scss'
import Login from './Containers/Login';
import Register from './Containers/Register';
import MarketCap from './Containers/MarketCap';
import Overview from './Containers/Overview';
import Wallet from './Containers/Wallet';
import Axios from 'axios';
import './Styles/main.scss'

function App({history}) {
  useEffect(() => {
    const token = localStorage.getItem('token')
    if(token){
      Axios.defaults.headers.common['token'] = token // for all requests
    }else{
      history.push('/login')
    }
  }, [])
  return (
    <>
      <Switch>
        <Route path="/login" component={Login}/>
        <Route path="/register" component={Register}/>
        <Route path="/marketcap" component={MarketCap}/>
        <Route path="/overview" component={Overview}/>
        <Route path="/wallet" component={Wallet}/>
        <Route path="/" component={Exchange}/>
      </Switch>
    </>
  );
}

export default withRouter(App);
