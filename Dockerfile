FROM node:10

# Setting working directory. All the path will be relative to WORKDIR
WORKDIR /app/

# Installing dependencies
COPY package*.json /app/
RUN npm install
RUN npm i serve -g

# Copying source files
COPY . /app/

# Building app
RUN npm run build

EXPOSE 3000

# Running the app
CMD [ "serve", "-s", "build", "-l", "3000"]